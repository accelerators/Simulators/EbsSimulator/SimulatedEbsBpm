/*----- PROTECTED REGION ID(SimulatedEbsBpmStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        SimulatedEbsBpmStateMachine.cpp
//
// description : State machine file for the SimulatedEbsBpm class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SimulatedEbsBpmStateMachine.cpp
#include <tango/classes/SimulatedEbsBpm/SimulatedEbsBpm.h>

//================================================================
//  States  |  Description
//================================================================
//  ON      |  
//  FAULT   |  
//  OFF     |  


namespace SimulatedEbsBpm_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_TBT_Enable_allowed()
 * Description:  Execution allowed for TBT_Enable attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_TBT_Enable_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for TBT_Enable attribute in Write access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_EnableStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_EnableStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_EnableStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_EnableStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_TBT_BufSize_allowed()
 * Description:  Execution allowed for TBT_BufSize attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_TBT_BufSize_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for TBT_BufSize attribute in Write access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_BufSizeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_BufSizeStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_BufSizeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_BufSizeStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SA_HPositions_Peak_allowed()
 * Description:  Execution allowed for SA_HPositions_Peak attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SA_HPositions_Peak_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::SA_HPositions_PeakStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SA_HPositions_PeakStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SA_VPositions_Peak_allowed()
 * Description:  Execution allowed for SA_VPositions_Peak attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SA_VPositions_Peak_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::SA_VPositions_PeakStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SA_VPositions_PeakStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SA_HPositions_RMS_allowed()
 * Description:  Execution allowed for SA_HPositions_RMS attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SA_HPositions_RMS_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::SA_HPositions_RMSStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SA_HPositions_RMSStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SA_VPositions_RMS_allowed()
 * Description:  Execution allowed for SA_VPositions_RMS attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SA_VPositions_RMS_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::SA_VPositions_RMSStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SA_VPositions_RMSStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_IncoherencyCheck_allowed()
 * Description:  Execution allowed for IncoherencyCheck attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_IncoherencyCheck_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for IncoherencyCheck attribute in Write access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::IncoherencyCheckStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::IncoherencyCheckStateAllowed_WRITE

	//	Not any excluded states for IncoherencyCheck attribute in read access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::IncoherencyCheckStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::IncoherencyCheckStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_States_allowed()
 * Description:  Execution allowed for States attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_States_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for States attribute in read access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::StatesStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::StatesStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_All_Status_allowed()
 * Description:  Execution allowed for All_Status attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_All_Status_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for All_Status attribute in read access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::All_StatusStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::All_StatusStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_All_BPIStation_allowed()
 * Description:  Execution allowed for All_BPIStation attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_All_BPIStation_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for All_BPIStation attribute in read access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::All_BPIStationStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::All_BPIStationStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_All_SA_HPosition_allowed()
 * Description:  Execution allowed for All_SA_HPosition attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_All_SA_HPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::All_SA_HPositionStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::All_SA_HPositionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_All_SA_VPosition_allowed()
 * Description:  Execution allowed for All_SA_VPosition attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_All_SA_VPosition_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::All_SA_VPositionStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::All_SA_VPositionStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SA_Incoherencies_allowed()
 * Description:  Execution allowed for SA_Incoherencies attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SA_Incoherencies_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::SA_IncoherenciesStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SA_IncoherenciesStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SA_Sums_allowed()
 * Description:  Execution allowed for SA_Sums attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SA_Sums_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::SA_SumsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SA_SumsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_TriggerCounters_allowed()
 * Description:  Execution allowed for TriggerCounters attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_TriggerCounters_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::TriggerCountersStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TriggerCountersStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_HOffsets_allowed()
 * Description:  Execution allowed for HOffsets attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_HOffsets_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for HOffsets attribute in Write access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::HOffsetsStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::HOffsetsStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::HOffsetsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::HOffsetsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_VOffsets_allowed()
 * Description:  Execution allowed for VOffsets attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_VOffsets_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for VOffsets attribute in Write access.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::VOffsetsStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::VOffsetsStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::VOffsetsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::VOffsetsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_TBT_HPositions_allowed()
 * Description:  Execution allowed for TBT_HPositions attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_TBT_HPositions_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_HPositionsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_HPositionsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_TBT_VPositions_allowed()
 * Description:  Execution allowed for TBT_VPositions attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_TBT_VPositions_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_VPositionsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_VPositionsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_TBT_Sums_allowed()
 * Description:  Execution allowed for TBT_Sums attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_TBT_Sums_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(SimulatedEbsBpm::TBT_SumsStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::TBT_SumsStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_Reset_allowed()
 * Description:  Execution allowed for Reset attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_Reset_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::ON ||
		get_state()==Tango::OFF)
	{
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::ResetStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::ResetStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_Devicename2Indice_allowed()
 * Description:  Execution allowed for Devicename2Indice attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_Devicename2Indice_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Devicename2Indice command.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::Devicename2IndiceStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::Devicename2IndiceStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_GetBPMName_allowed()
 * Description:  Execution allowed for GetBPMName attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_GetBPMName_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for GetBPMName command.
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::GetBPMNameStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::GetBPMNameStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SetOneBpmIncoherent_allowed()
 * Description:  Execution allowed for SetOneBpmIncoherent attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SetOneBpmIncoherent_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::SetOneBpmIncoherentStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SetOneBpmIncoherentStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_ResetIncoherentBpm_allowed()
 * Description:  Execution allowed for ResetIncoherentBpm attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_ResetIncoherentBpm_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::ResetIncoherentBpmStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::ResetIncoherentBpmStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_SetOneBpmDisabled_allowed()
 * Description:  Execution allowed for SetOneBpmDisabled attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_SetOneBpmDisabled_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::SetOneBpmDisabledStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SetOneBpmDisabledStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedEbsBpm::is_ResetDisabledBpm_allowed()
 * Description:  Execution allowed for ResetDisabledBpm attribute
 */
//--------------------------------------------------------
bool SimulatedEbsBpm::is_ResetDisabledBpm_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(SimulatedEbsBpm::ResetDisabledBpmStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::ResetDisabledBpmStateAllowed
		return false;
	}
	return true;
}


/*----- PROTECTED REGION ID(SimulatedEbsBpm::SimulatedEbsBpmStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	SimulatedEbsBpm::SimulatedEbsBpmStateAllowed.AdditionalMethods

}	//	End of namespace
