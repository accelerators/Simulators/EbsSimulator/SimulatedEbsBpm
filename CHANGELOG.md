# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [6.0.0] - 08-01-2020

### Changed
* Att name from SA_HPositions to All_SA_HPosition
* Att name from SA_VPositions to All_SA_VPosition
* Cmd name from GetBPMindex to devicename2Indice which now returns a DevShort
* Individual BPM name as also changed (srdiag/bpm/cXX-YY)

## [5.0.0] - 22-11-2018

### Changed

* It's now possible to have one BPM disabled (only one). 
This required two new commands
    * SetOneBpmDisabled which returns the index of the disabled BPM
    * ResetDisabledBpm
* It's now possible to have one BPM incoherent (only one).
This requires two new commands:
    * SetOneBpmIncoherent which returns the index of the incoherent BPM
    * ResetIncoherentBpm
* Add some attributes:
    * SA_Incoherencies
    * SA_Sums
    * TBT_Sums
    * TriggerCounters
    
### Fixed Bugs

* HOffset and VOffset attributes: At init, their set values were not set

## [4.0.0] - 04-10-2018

### Changed

* Rename several attributes to be on-line with the real BPM all device
    * HPositions is now SA_HPositions
    * VPositions is now SA_VPositions
    * HPositions_Peak is now SA_HPositions_Peak
    * VPositions_Peak is now SA_VPositions_Peak
    * HPositions_RMS is now SA_HPositions_RMS
    * VPositions_RMS is now SA_VPositions_RMS
    * HPositionsTbT is now TBT_HPositions
    * VPositionsTbT is now TBT_VPositions
    * TbT_Enable is now TBT_Enable
    * TbT_BufferSize is now TBT_BufSize

## [3.0.0] - 26-09-2018

### Changed

* Flip X and Y data for attributes HPositionsTbT and VPositionsTbT

## [2.0.0] - 20-06-2018

### Added
* HOffsets and VOffsets attributes to simulate BPM offset. The returned orbits are the orbits computed by AT plus those offsets
* Official ESRF .md files (README and CHANGELOG)
